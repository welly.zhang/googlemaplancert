import React, {useState, useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import MapWithMarker from './MapWithMarker'
import Aech from './boundary-geodata/IDN/ADM4/Aceh.json'
import NativeSelect from '@material-ui/core/NativeSelect';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import CssBaseline from '@material-ui/core/CssBaseline';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';

import _  from 'lodash';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    width:'80%'
  },
  selectContainer:{
    marginTop:'15px',
    marginBottom:'15px'
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  }
}));



function App() {

  const classes = useStyles();

  const [province, setProvince] = useState('')
  const [city, setCity] = useState('')
  const [district, setDistrit] = useState('')
  const [village, setVillage] = useState('')

  const [polygonPath, setPolygonPath] = useState([])

  const [provinceOption, setProvinceOption] = useState([])
  const [cityOption, setCityOption] = useState([])
  const [districtOption, setDistritOption] = useState([])
  const [villageOption, setVillageOption] = useState([])

  const [isMutiplePolygon, setIsMutiplePolygon] = useState(false)
  
  useEffect(()=>{
      const selectData = Aech.features.map(x=>
        {
          return { 
            ADM1_EN:x.properties.ADM1_EN,
            ADM2_EN:x.properties.ADM2_EN,
            ADM3_EN:x.properties.ADM3_EN,
            ADM4_EN:x.properties.ADM4_EN
          }
        }
      )
      
      const provinceOption = _.uniq(selectData.map(x=>x['ADM1_EN']))
      setProvinceOption(provinceOption)
      setProvince(provinceOption[0])

      const cityOption  = _.uniq(selectData.filter(x=> x['ADM1_EN'] === provinceOption[0]).map(x=>x['ADM2_EN']))
      setCityOption (cityOption)
      setCity(cityOption[0])

      const disOption  = _.uniq(selectData.filter(x=> x['ADM1_EN'] === provinceOption[0] && x['ADM2_EN'] === cityOption[0]).map(x=>x['ADM3_EN']))
      setDistritOption (disOption)
      setDistrit(disOption[0])

      const villageOption = _.uniq(selectData.filter(x=> x['ADM1_EN'] === provinceOption[0] && x['ADM2_EN'] === cityOption [0] && x['ADM3_EN'] === disOption[0]).map(x=>x['ADM4_EN']))
      setVillageOption (villageOption)
      setVillage(villageOption[0])

      const defaultPolygon = {ADM1_EN: provinceOption[0], ADM2_EN: cityOption[0], ADM3_EN: disOption[0], ADM4_EN: villageOption[0] }
      drawPolygon(defaultPolygon)

  },[])

  const provinceChange = (_province) => {
    setProvince(_province)
  }
  const cityChange = (_city) => {
    const selectData = Aech.features.map(x=>
      {
        return { 
          ADM1_EN:x.properties.ADM1_EN,
          ADM2_EN:x.properties.ADM2_EN,
          ADM3_EN:x.properties.ADM3_EN,
          ADM4_EN:x.properties.ADM4_EN
        }
      }
    )

    setCity(_city)

    const disOption  = _.uniq(selectData.filter(x=> x['ADM1_EN'] === province && x['ADM2_EN'] === _city).map(x=>x['ADM3_EN']))
    setDistritOption (disOption)
    setDistrit(disOption[0])

    const villageOption = _.uniq(selectData.filter(x=> x['ADM1_EN'] === province&& x['ADM2_EN'] === _city && x['ADM3_EN'] === disOption[0]).map(x=>x['ADM4_EN']))
    setVillageOption (villageOption)
    setVillage(villageOption[0])

    const defaultPolygon = {ADM1_EN: province, ADM2_EN: _city, ADM3_EN: disOption[0], ADM4_EN: villageOption[0] }
    drawPolygon(defaultPolygon)
  }
  const districtChange = (_district) => {
    const selectData = Aech.features.map(x=>
      {
        return { 
          ADM1_EN:x.properties.ADM1_EN,
          ADM2_EN:x.properties.ADM2_EN,
          ADM3_EN:x.properties.ADM3_EN,
          ADM4_EN:x.properties.ADM4_EN
        }
      }
    )

    setDistrit(_district)

    const villageOption = _.uniq(selectData.filter(x=> x['ADM1_EN'] === province&& x['ADM2_EN'] === city && x['ADM3_EN'] === _district).map(x=>x['ADM4_EN']))
    setVillageOption (villageOption)
    setVillage(villageOption[0])

    const defaultPolygon = {ADM1_EN: province, ADM2_EN: city, ADM3_EN: _district, ADM4_EN: villageOption[0] }
    drawPolygon(defaultPolygon)
  }
  const villageChange = (_village) => {
    setVillage(_village)
    const defaultPolygon = {ADM1_EN: province, ADM2_EN: city, ADM3_EN: district, ADM4_EN: _village }
    drawPolygon(defaultPolygon)
  }

  
  const drawPolygon = ( areaDict ) => {
    const defaultPolygonData = Aech.features.filter(x=> 
        x.properties.ADM1_EN===areaDict['ADM1_EN'] &&
        x.properties.ADM2_EN===areaDict['ADM2_EN'] &&
        x.properties.ADM3_EN===areaDict['ADM3_EN'] &&
        x.properties.ADM4_EN===areaDict['ADM4_EN'] 
    )[0]
    
    const cordinate = defaultPolygonData.geometry.coordinates
    
    setPolygonPath(cordinate)
    setIsMutiplePolygon(defaultPolygonData.geometry.type === 'Polygon' ? false : true)
  }

  return (
    
    <Grid container direction={'row'} >
      <Grid item  lg={6} md={6} sm={12} xs={12}>
        <Grid container direction={'row'} justify='center'>
          <Grid item  lg={12}>
            <Container component="main" maxWidth="xs">
              <CssBaseline />
              <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                  <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                  Indonesia Land Cert OCR Poc
                </Typography>
              </div>
            </Container>
          </Grid>
          <Grid item lg={6} className={classes.selectContainer}>
            <Grid container justify='center'>
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="Province">Province</InputLabel>
                  <NativeSelect
                    inputProps={{
                      name: 'Province',
                      id: 'Province',
                    }}
                    value={province}
                    onChange={(event)=>
                      {
                        provinceChange(event.target.value)
                      }
                    }
                  >
                    {provinceOption.map(x=>{
                      return <option key={x} value={x}>{x}</option>
                    })}
                  </NativeSelect>
              </FormControl>
            </Grid>
          </Grid>
          <Grid item lg={6} className={classes.selectContainer}>
            <Grid container justify='center'>
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="City">City</InputLabel>
                  <NativeSelect
                    inputProps={{
                      name: 'City',
                      id: 'City',
                    }}
                    value={city}
                    onChange={(event)=>
                      {
                        cityChange(event.target.value)
                      }
                    }
                  >
                    {cityOption.map(x=>{
                      return <option key={x} value={x}>{x}</option>
                    })}
                  </NativeSelect>
              </FormControl>
            </Grid>
          </Grid>
        </Grid>
        <Grid container direction={'row'}>

          <Grid item lg={6} className={classes.selectContainer}>
            <Grid container justify='center'>
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="District">District</InputLabel>
                  <NativeSelect
                    inputProps={{
                      name: 'District',
                      id: 'District',
                    }}
                    value={district}
                    onChange={(event)=>
                      {
                        districtChange(event.target.value)
                      }
                    }
                  >
                    {districtOption.map(x=>{
                      return <option key={x} value={x}>{x}</option>
                    })}
                  </NativeSelect>
              </FormControl>
            </Grid>

          </Grid>
          <Grid item lg={6} className={classes.selectContainer}>
            <Grid container justify='center'>
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="village">village</InputLabel>
                  <NativeSelect
                    inputProps={{
                      name: 'village',
                      id: 'village',
                    }}
                    value={village}
                    onChange={(event)=>
                      {
                        villageChange(event.target.value)
                      }
                    }
                  >
                    {villageOption.map(x=>{
                      return <option key={x} value={x}>{x}</option>
                    })}
                  </NativeSelect>
              </FormControl>
            </Grid>  
          </Grid>
        </Grid>
        <Grid container direction={'column'} alignItems='center'>
          <Grid item style={{marginTop:'30px'}}>
            <Typography component="h1" variant="h6">
               Polygon (in json format)
            </Typography>
          </Grid>
          <Grid item container justify='center' style={{marginTop:'30px'}}>
            <Grid item style={{width:'70%'}}>
              <TextField
                  placeholder=""
                  multiline
                  rows={6}
                  rowsMax={10}
                  style={{width:'100%'}}
                />
            </Grid>
          </Grid>
         
        </Grid>
      </Grid> 
      <Grid item  lg={6} md={6} sm={12} xs={12} >
          <MapWithMarker
              googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyByL9wj2LyG1CEO031AOYfnpFoRFy5Frxo&v=3.exp&libraries=geometry,drawing,places"
              loadingElement={<div style={{ height: `50%` }} />}
              containerElement={<div style={{ height: `95vh` }} />}
              mapElement={<div style={{ width:'100%', height: `95vh` }} />}
              polygonPath={polygonPath}
              isMutiple={isMutiplePolygon}
              infoWording={`${district} / ${village}`}
            />
      </Grid>
    
    </Grid>
   
  );
}

export default App;
