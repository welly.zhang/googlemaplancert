import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
    Polygon,
    InfoWindow,
    Marker
  } from "react-google-maps";

import React, { useRef, useCallback, useEffect, useState } from "react";
import _ from 'lodash'

const MapWithMarker = withScriptjs(withGoogleMap(props =>{
  
  const polygonRef = useRef(null);
  const { polygonPath, isMutiple} =  props

  const [zoomPoint , setZoomPoint] = useState() 
  const [polygonData , setPolygonData] = useState([]) 

  const onLoad = useCallback(
    polygon => {
      polygonRef.current = polygon;

    },
    []
  );

  // Fit bounds function
  const fitBounds = () => {
    const bounds = new window.google.maps.LatLngBounds();
  
    if(polygonData?.length > 0)
    {
      polygonData[0].map(item => {
        bounds.extend(item);
        return item
      });
      
      polygonRef.current.fitBounds(bounds);
    }
   
  };

  const handlingCordinates = () => {

    const polygonList = []
    let zoomPoint = {}

    if(isMutiple == false)
    {
      const tempPolygon = []
      polygonPath[0].map(x=>{
          tempPolygon.push({lat: x[1], lng:x[0]})
      })
      polygonList.push(tempPolygon)
      zoomPoint =  _.maxBy(polygonList[0], x => x.lat)
    }
    else
    {
      polygonPath.map(x=>{
         const tempPolygon = []
         x[0].map(y=>{
            tempPolygon.push({lat: y[1], lng:y[0]})
         })
         polygonList.push(tempPolygon)
      })
      
      const latData = _.concat(polygonList[0].map(x=>x.lat), polygonList[1].map(x=>x.lat))
      const lngData = _.concat(polygonList[0].map(x=>x.lng), polygonList[1].map(x=>x.lng))
      const lat = _.mean([_.max(latData),_.min(latData)])
      const lng = _.mean([_.max(lngData),_.min(lngData)])
 
      zoomPoint =  { lat:lat, lng:lng }
    }

    setPolygonData(polygonList)
    setZoomPoint(zoomPoint)
  }
  
  useEffect(()=> {
    handlingCordinates();
  },[polygonPath])
  // Fit bounds on mount, and when the markers change
  useEffect(() => {
    fitBounds();
  }, [polygonData]);
  
  return (
      <GoogleMap
          defaultZoom={7}
          defaultCenter={zoomPoint}
          center={zoomPoint}
          zoom={2}
          ref={polygonRef} 
      >
         <Marker
          key={zoomPoint}
          position={zoomPoint}
          visible={false}
        >
          <InfoWindow >
            <div>
              {props.infoWording}
            </div>
          </InfoWindow>
        </Marker>
        {
          polygonData.map((x)=>{
            return(
              <Polygon
                // Make the Polygon editable / draggable
                //editable
                //draggable
                path={x}
                onLoad={onLoad}
                options={{ strokeOpacity: 2, strokeColor: "	#CE0000", fillColor:"	#CE0000"}}
              />
            )
          })
        }


        

      </GoogleMap>
  )
}
));

export default MapWithMarker